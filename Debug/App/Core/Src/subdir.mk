################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../App/Core/Src/mlm32l0xx_hal_msp.c \
../App/Core/Src/mlm32l0xx_hw.c \
../App/Core/Src/mlm32l0xx_it.c 

OBJS += \
./App/Core/Src/mlm32l0xx_hal_msp.o \
./App/Core/Src/mlm32l0xx_hw.o \
./App/Core/Src/mlm32l0xx_it.o 

C_DEPS += \
./App/Core/Src/mlm32l0xx_hal_msp.d \
./App/Core/Src/mlm32l0xx_hw.d \
./App/Core/Src/mlm32l0xx_it.d 


# Each subdirectory must supply rules for building sources it contributes
App/Core/Src/%.o App/Core/Src/%.su: ../App/Core/Src/%.c App/Core/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DUSE_HAL_DRIVER -DDEBUG -DSTM32L072xx -DUSE_B_L072Z_LRWAN1 -DREGION_IN865 -DLOW_POWER_DISABLE -c -I../Drivers/CMSIS/Device/ST/STM32L0xx/Include -I../Drivers/CMSIS/Include -I../Drivers/STM32L0xx_HAL_Driver/Inc -I../App/Core/Inc -I../App/LoRaWAN/inc -I../Drivers/BSP/CMWX1ZZABZ-0xx -I../Drivers/Middlewares/LoRaWAN/Crypto -I../Drivers/Middlewares/LoRaWAN/Mac -I../Drivers/Middlewares/LoRaWAN/Mac/region -I../Drivers/Middlewares/LoRaWAN/Phy -I../Drivers/Middlewares/LoRaWAN/Utilities -I../Drivers/BSP/B-L072Z-LRWAN1 -I../Drivers/BSP/Components/sx1276 -I../Drivers/Middlewares/LoRaWAN/Patterns/Basic -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-App-2f-Core-2f-Src

clean-App-2f-Core-2f-Src:
	-$(RM) ./App/Core/Src/mlm32l0xx_hal_msp.d ./App/Core/Src/mlm32l0xx_hal_msp.o ./App/Core/Src/mlm32l0xx_hal_msp.su ./App/Core/Src/mlm32l0xx_hw.d ./App/Core/Src/mlm32l0xx_hw.o ./App/Core/Src/mlm32l0xx_hw.su ./App/Core/Src/mlm32l0xx_it.d ./App/Core/Src/mlm32l0xx_it.o ./App/Core/Src/mlm32l0xx_it.su

.PHONY: clean-App-2f-Core-2f-Src

