################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Drivers/BSP/Components/sx1276/sx1276.c 

OBJS += \
./Drivers/BSP/Components/sx1276/sx1276.o 

C_DEPS += \
./Drivers/BSP/Components/sx1276/sx1276.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/BSP/Components/sx1276/%.o Drivers/BSP/Components/sx1276/%.su: ../Drivers/BSP/Components/sx1276/%.c Drivers/BSP/Components/sx1276/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DUSE_HAL_DRIVER -DDEBUG -DSTM32L072xx -DUSE_B_L072Z_LRWAN1 -DREGION_IN865 -DLOW_POWER_DISABLE -c -I../Drivers/CMSIS/Device/ST/STM32L0xx/Include -I../Drivers/CMSIS/Include -I../Drivers/STM32L0xx_HAL_Driver/Inc -I../App/Core/Inc -I../App/LoRaWAN/inc -I../Drivers/BSP/CMWX1ZZABZ-0xx -I../Drivers/Middlewares/LoRaWAN/Crypto -I../Drivers/Middlewares/LoRaWAN/Mac -I../Drivers/Middlewares/LoRaWAN/Mac/region -I../Drivers/Middlewares/LoRaWAN/Phy -I../Drivers/Middlewares/LoRaWAN/Utilities -I../Drivers/BSP/B-L072Z-LRWAN1 -I../Drivers/BSP/Components/sx1276 -I../Drivers/Middlewares/LoRaWAN/Patterns/Basic -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Drivers-2f-BSP-2f-Components-2f-sx1276

clean-Drivers-2f-BSP-2f-Components-2f-sx1276:
	-$(RM) ./Drivers/BSP/Components/sx1276/sx1276.d ./Drivers/BSP/Components/sx1276/sx1276.o ./Drivers/BSP/Components/sx1276/sx1276.su

.PHONY: clean-Drivers-2f-BSP-2f-Components-2f-sx1276

